#ifndef HOTELREPORT_H
#define HOTELREPORT_H

#include "Transaction.h"
#include <vector>
#include <sstream>
#include <map>
#include "RoomReservation.h"

struct TransactionResult {
    Transaction transac;
    std::string result;
    int finishTime;

    TransactionResult() {}
    TransactionResult(const Transaction& t, const std::string& r)
    : transac(t), result(r), finishTime(-1) {}
};
typedef std::map<int,RoomReservation> ReservationCalendar;
typedef std::vector<ReservationCalendar> HotelRooms;
typedef std::vector<TransactionResult> TransactionLog;
typedef std::vector<std::string> StringVec;

void reportTransactionLog(const TransactionLog&);
void reportRoomStatus(const HotelRooms&, const StringVec&);
void reportDeadlines(const TransactionLog&);

#endif
