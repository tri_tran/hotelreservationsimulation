#include "Customer.h"

Customer::Customer(int cid, int reserveTime, int cancelTime, int checkTime, int payTime)
: cid(cid), reserveTime(reserveTime), cancelTime(cancelTime), checkTime(checkTime),
  payTime(payTime), commands(), cmdId(0)
{

}

void Customer::addCommand(const std::string& cmd)
{
    std::ostringstream oss;
    oss << cmd << " " << cid;
    commands.push_back(oss.str());
}

bool Customer::hasNextCommand()const
{
    return cmdId < commands.size();
}

void Customer::sendCommand(Pipe& pipe, time_t issueTime)const
{
    std::ostringstream msgStream;
    msgStream << commands[cmdId] << " " << issueTime;
    write(pipe.writeEnd(), msgStream.str().c_str(), msgStream.str().length());
}

void Customer::nextTransaction()
{
    cmdId++;
}

std::ostream& operator<<(std::ostream& out, const Customer& c)
{
    out << "(" << c.reserveTime << "," << c.cancelTime
        << "," << c.checkTime << "," << c.payTime << ")\n";
    for (size_t i = 0; i < c.commands.size(); ++i)
        out << c.commands[i] << "\n";
    return out;
}
