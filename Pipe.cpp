#include "Pipe.h"

Pipe::Pipe()
{
    isOpened[0] = isOpened[1] = false;
}

void Pipe::create()
{
    if (isOpened[0] || isOpened[1])
    {
        std::cerr << "Pipe already created\n";
        exit(1);
    }
    if (pipe(fd) == -1)
    {
        std::cerr << "Error creating pipe\n";
        exit(1);
    }
    isOpened[0] = isOpened[1] = true;
}

void Pipe::closeAllEnds()
{
    closeReadEnd();
    closeWriteEnd();
}

void Pipe::closeReadEnd()
{
    if (!isOpened[0]) return;
    close(fd[0]);
    isOpened[0] = false;
}

void Pipe::closeWriteEnd()
{
    if (!isOpened[1]) return;
    close(fd[1]);
    isOpened[1] = false;
}

int Pipe::readEnd()const
{
    if (!isOpened[0])
    {
        std::cerr << "Attempt to access already closed read end\n";
        exit(1);
    }
    return fd[0];
}

int Pipe::writeEnd()const
{
    if (!isOpened[1])
    {
        std::cerr << "Attempt to access already closed write end\n";
        exit(1);
    }
    return fd[1];
}
