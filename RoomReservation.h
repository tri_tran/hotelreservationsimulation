#ifndef ROOMRESERVATION_H
#define ROOMRESERVATION_H

#include <map>
#include <vector>
#include <string>
#include <iostream>

struct RoomReservation {
    int rid, startDate, duration, cid;
    bool paid;

    RoomReservation(int rid=-1, int startDate=-1, int duration=-1, int cid=-1, bool paid=false)
    : rid(rid), startDate(startDate), duration(duration), cid(cid), paid(paid) {}
    int endDate()const { return startDate + duration; }
    bool similar(const RoomReservation&)const;
};

bool isReservable(const RoomReservation&, const std::map<int,RoomReservation>&);
bool isPayable(const RoomReservation&, const std::map<int,RoomReservation>&);
bool isCancellable(const RoomReservation&, const std::map<int,RoomReservation>&);
void print(std::ostream&, const RoomReservation&, const std::vector<std::string>&);

#endif // ROOMRESERVATION_H
