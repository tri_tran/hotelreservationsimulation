#include "HotelReport.h"

void reportTransactionLog(const TransactionLog& transacLog)
{
    std::cout << "\n\n\n\n========================== BEGIN - TRANSACTION LOG ==========================\n";
    for (size_t i = 0; i < transacLog.size(); ++i)
    {
        std::cout << "Transaction #" << i+1 << "\n";
        std::cout << "Description:\n" << transacLog[i].transac;
        std::cout << "Result:\n  " << transacLog[i].result << "\n";
        if (i+1 < transacLog.size())
            std::cout << "-----------------------------------------------------------------------------\n";
    }
    std::cout << "==========================  END - TRANSACTION LOG  ==========================\n";
}

void reportRoomStatus(const HotelRooms& hotelRooms, const StringVec& custNames)
{
    std::cout << "\n\n\n\n============================ BEGIN - ROOMS STATUS ===========================\n";
    for (size_t i = 1; i < hotelRooms.size(); ++i)
    {
        std::cout << "Room #" << i << "\n";
        if (hotelRooms[i].empty())
        {
            std::cout << "<No reservations>\n";
        }
        else
        {
            std::cout << "Reservation(s):\n";
            for (ReservationCalendar::const_iterator it = hotelRooms[i].begin();
                 it != hotelRooms[i].end(); ++it)
            {
                std::cout << "    " << (it->second.paid ? " PAID " : "UNPAID")
                          << "    from " << it->second.startDate
                          << " to " << it->second.startDate + it->second.duration
                          << "    Customer #" << it->second.cid
                          << " (" << custNames[it->second.cid] << ")\n";
            }
        }
        if (i+1 < hotelRooms.size())
            std::cout << "-----------------------------------------------------------------------------\n";
    }
    std::cout << "============================= END - ROOMS STATUS ===========================\n";
}

void reportDeadlines(const TransactionLog& transacLog)
{
    std::cout << "\n\n\n\n========================== BEGIN - DEADLINE REPORT ==========================\n";
    for (size_t i = 0; i < transacLog.size(); ++i)
    {
        const TransactionResult& res = transacLog[i];
        std::cout << "Transaction #" << i+1 << "\n";
        std::cout << "Deadline: " << res.transac.getHardDeadline() << "\n";
        std::cout << "  Finish: " << res.finishTime << "\n";
        int lateness = res.finishTime - res.transac.getHardDeadline();
        if (lateness <= 0)
            std::cout << "MEET deadline.\n";
        else
            std::cout << "MISS deadline by " << lateness << "ms.\n";
        if (i+1 < transacLog.size())
            std::cout << "-----------------------------------------------------------------------------\n";
    }
    std::cout << "==========================  END - DEADLINE REPORT  ==========================\n";
}
