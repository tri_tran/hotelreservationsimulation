#ifndef PIPE_H
#define PIPE_H

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <sys/types.h>

class Pipe
{
public:
    Pipe();
    void create();
    void closeAllEnds();
    void closeReadEnd();
    void closeWriteEnd();
    int readEnd()const;
    int writeEnd()const;
private:
    int fd[2];
    bool isOpened[2];
};

#endif
