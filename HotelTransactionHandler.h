#ifndef HOTELTRANSACTIONHANDLER_H
#define HOTELTRANSACTIONHANDLER_H

#include "HotelReport.h"

int checkReservationConflict(const Transaction&, const HotelRooms&);
TransactionResult handleReservation(const Transaction&, HotelRooms&);
int checkCancellationConflict(const Transaction&, const HotelRooms&);
TransactionResult handleCancellation(const Transaction&, HotelRooms&);
TransactionResult handleChecking(const Transaction&, const HotelRooms&, const StringVec&);
int checkPaymentConflict(const Transaction&, const HotelRooms&);
TransactionResult handlePayment(const Transaction&, HotelRooms&);

#endif // HOTELSERVER_H
