#include "Semaphore.h"

void Semaphore::create(int key, int semnum, int nsops)
{
    this->nsops = nsops;
    this->sb.resize(nsops);
    semid = semget(key, semnum, IPC_CREAT);
    if (semid < 0)
    {
        std::cerr << "Error creating semaphore.\n";
        exit(1);
    }
}

void Semaphore::release()
{
    if (semctl(semid, 0, IPC_RMID) < 0)
    {
        std::cerr << "Error deleting semaphore.\n";
        exit(1);
    }
}

void Semaphore::setSemValue(int semnum, int value)
{
    if (semctl(semid, semnum, SETVAL, value) < 0)
    {
        std::cerr << "Cannot set value of semaphore.\n";
        exit(1);
    }
}

void Semaphore::prepareWait(int i, int semnum)
{
    sb[i].sem_num = semnum;
    sb[i].sem_op = -1;
    sb[i].sem_flg = 0;
}

void Semaphore::prepareSignal(int i, int semnum)
{
    sb[i].sem_num = semnum;
    sb[i].sem_op =  1;
    sb[i].sem_flg = 0;
}

void Semaphore::run(int count)
{
    semop(semid, &sb[0], count);
}
