#include <iostream>
#include <fstream>
#include "Customer.h"
#include "Semaphore.h"
#include "Pipe.h"
#include "HotelTransactionHandler.h"
#include <limits>

const int KEY = 1017823;

void safeOpen(std::ifstream&, const std::string&);
void readInput(int&, int&, std::vector<Customer>&);

void hotelServer(Semaphore&, std::vector<Pipe>&, int&, StringVec&,
                 const std::vector<Customer>&, HotelRooms&, TransactionLog&);
void customerClient(Semaphore&, Pipe&, Customer&);

int main()
{
    int nRooms, nCustomers;
    std::vector<Customer> customers;
    Semaphore sem;
    std::vector<Pipe> pipes;
    int customerId = 0;
    int pid = -1;
    StringVec customerNames;
    HotelRooms hotelRooms;
    TransactionLog transactionLog;

    // Read input
    readInput(nRooms, nCustomers, customers);
    customerNames.resize(nCustomers + 1, "");
    hotelRooms.resize(nRooms + 1);

    // Create pipe and set semaphores' initial values
    pipes.resize(nCustomers);
    sem.create(KEY, nCustomers, 1);
    for (int i = 0; i < nCustomers; ++i)
    {
        pipes[i].create();
        sem.setSemValue(i, 1); //set all semaphores' initial values to 1
    }

    // Fork all customers
    for (customerId = 0; customerId < nCustomers; ++customerId)
    {
        if ((pid = fork()) == -1)
        {
            std::cerr << "Fork error.\n";
            exit(1);
        }
        else if (pid == 0)
            break;
    }

    //////////////////// HOTEL SIMULATION ////////////////////

    //------------------------------------------------------//
    //---------------------- Customers ---------------------//
    //------------------------------------------------------//
    if (pid == 0)
    {
        for (size_t i = 0; i < pipes.size(); ++i)
            if ((int)i != customerId)
                pipes[i].closeAllEnds();

        customerClient(sem, pipes[customerId], customers[customerId]);

        exit(0);
    }

    //------------------------------------------------------//
    //----------------------   Hotel  ----------------------//
    //------------------------------------------------------//
    hotelServer(sem, pipes, nCustomers, customerNames, customers, hotelRooms, transactionLog);

    reportTransactionLog(transactionLog);
    reportRoomStatus(hotelRooms, customerNames);
    reportDeadlines(transactionLog);
}



void customerClient(Semaphore& sem, Pipe& pipe, Customer& customer)
{
    int cid = customer.getCustomerId();
    pipe.closeReadEnd();
    while (customer.hasNextCommand())
    {
        time_t issueTime = time(NULL);

        // WAIT(s_i)
        sem.prepareWait(0, cid - 1); //set op 0 to wait on s_i
        sem.run(1); //run only first op (wait)

        customer.sendCommand(pipe, issueTime);
        customer.nextTransaction();
    }
    pipe.closeWriteEnd();
}

int getEdfIndex(const std::vector<Transaction>& trans)
{
    int edfIndex = -1;
    time_t edf = std::numeric_limits<time_t>::max();
    for (size_t i = 0; i < trans.size(); ++i)
    {
        if (trans[i].getType() == Transaction::None) continue;
        if (trans[i].getHardDeadline() < edf)
        {
            edf = trans[i].getHardDeadline();
            edfIndex = i;
        }
    }
    return edfIndex;
}

void hotelServer(Semaphore& sem, std::vector<Pipe>& pipes, int& nCustomers,
                 StringVec& customerNames, const std::vector<Customer>& customers,
                 HotelRooms& hotelRooms, TransactionLog& transactionLog)
{
    for (size_t i = 0; i < pipes.size(); ++i)
        pipes[i].closeWriteEnd();

    std::vector<int> cids; //0 1 2 ...
    for (int i = 0; i < nCustomers; ++i)
        cids.push_back(i);

    std::vector<Transaction> trans(nCustomers);

    time_t timer = time(NULL);
    while (nCustomers)
    {
        // read command(s)
        char buf[80];
        for (size_t i = 0; i < cids.size(); ++i)
        {
            int cid = cids[i];
            int nread = read(pipes[cid].readEnd(), buf, 80);
            buf[nread] = '\0';
            std::string command = buf;
            if (command.find("END") != command.npos)
            {
                trans[cid].reset();
                --nCustomers;
            }
            else
            {
                trans[cid].parse(command);
            }
        }

        if (!nCustomers) break;

        int edf = getEdfIndex(trans);

        Transaction& transac = trans[edf];

        std::cout << "Processing transaction #" << transactionLog.size() + 1 << "... ";
        std::cout.flush();

        // register 'new' customer (if new customer)
        if (customerNames[transac.getCustomerId()].empty())
        customerNames[transac.getCustomerId()] = transac.getCustomerName();

        // process transaction + simulate the execution time
        const Customer& customer = customers[transac.getCustomerId() - 1];
        TransactionResult tRes;
        int execTime = 0;
        switch (transac.getType())
        {
        case Transaction::Reserve:
            execTime = customer.getReserveTime();
            tRes = handleReservation(transac, hotelRooms);
            break;
        case Transaction::Cancel:
            execTime = customer.getCancelTime();
            tRes = handleCancellation(transac, hotelRooms);
            break;
        case Transaction::Check:
            execTime = customer.getCheckTime();
            tRes = handleChecking(transac, hotelRooms, customerNames);
            break;
        case Transaction::Pay:
            execTime = customer.getPayTime();
            tRes = handlePayment(transac, hotelRooms);
            break;
        default:
            break;
        }
        sleep(execTime);
        timer = time(NULL);
        tRes.finishTime = timer;
        transactionLog.push_back(tRes);
        std::cout << "Done!\n";

        cids.resize(1);
        cids[0] = edf;

        transac.reset();

        // SIGNAL(s_edf)
        sem.prepareSignal(0, edf); //set op 0 to signal on s_edf
        sem.run(1); //run only first op (signal)
    }


    for (size_t i = 0; i < pipes.size(); ++i)
        pipes[i].closeReadEnd();
    sem.release();
}

void readInput(int& nRooms, int& nCustomers, std::vector<Customer>& customers)
{
    std::ifstream fin;
    safeOpen(fin, "hw2input.txt");
    fin >> nRooms >> nCustomers;
    fin.ignore(100, '\n');
    for (int i = 1; i <= nCustomers; ++i)
    {
        std::string line;
        fin.ignore(100, '\n');
        int reserveTime, cancelTime, checkTime, payTime;
        fin >> line >> reserveTime >> line >> cancelTime
            >> line >> checkTime >> line >> payTime;
        fin.ignore(100, '\n');
        Customer customer(i, reserveTime, cancelTime, checkTime, payTime);
        while (std::getline(fin, line) && line.find("end") == line.npos)
        {
            customer.addCommand(line);
        }
        customer.addCommand("END"); ///////////
        customers.push_back(customer);
        fin.ignore(100, '\n');
    }
    fin.close();
}

void safeOpen(std::ifstream& fin, const std::string& fpath)
{
    fin.open(fpath.c_str());
    if (!fin)
    {
        std::cerr << "Cannot open file " << fpath << "\n";
        exit(1);
    }
}

